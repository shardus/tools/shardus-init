DEPRECATED

As per https://www.notion.so/shardus/a0e84bf31b97437eac5747d7940fb1c3?v=3bb4c72041194bb08a9fd5b3fcadb659&p=c19507d03e7d4386944120fad6334053,
we're deprecating this.

We decided it was too much effort to maintain the tool when you could achieve a similar effect simply running git clone.
