const { join, resolve } = require('path')
const execa = require('execa')
const del = require('del')

async function init (appName, {
  templateRepo,
  runInstall = true,
  stdio = [0, 1, 2]
} = {}) {
  if (!exists(appName)) throw new Error('shardusInit requires a valid appName to be given.')
  if (!exists(templateRepo)) throw new Error('shardusInit requires a templateRepo to be given.')
  const templatePath = resolve(__dirname, '../', 'tmp')

  let proc

  // Clone the template repo
  await del(templatePath, { force: true })
  proc = execa('git', ['clone', templateRepo, templatePath], {
    stdio
  })
  proc.catch(err => console.log(err))
  await proc

  // Create the new application folder from the template
  proc = execa('hygen', ['shardus', 'new', appName], {
    stdio,
    env: { HYGEN_TMPLS: join(templatePath, '_templates') },
    preferLocal: true,
    localDir: __dirname
  })
  proc.catch(err => console.log(err))
  await proc

  // Run `npm i` in the new app folder
  if (runInstall) {
    const appDir = join(process.cwd(), appName)
    proc = execa('npm', ['i'], {
      stdio,
      cwd: appDir
    })
    proc.catch(err => console.log(err))
    await proc
  }
}

function exists (thing) {
  return (typeof thing !== 'undefined' && thing !== null)
}

exports.init = init
