const lib = require('./lib')

async function init (args, options, logger) {
  await lib.init(args['appName'], {
    templateRepo: args['templateRepo'],
    runInstall: !options['noInstall']
  })
}

exports.init = init
