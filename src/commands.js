const actions = require('./actions')

const register = {
  init (prog, namespace) {
    prog
      .command(`${namespace ? namespace + ' ' : ''}init`, 'Create a new shardus project')
      .argument('<appName>', 'Project name')
      .argument('<templateRepo>', 'Git repo containing a template created with hygen')
      .option('--no-install', 'Do not run npm install in the new sample project folder')
      .action(actions.init)
  }
}

module.exports = register
